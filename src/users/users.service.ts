import { Injectable } from '@nestjs/common';

export type User = any;
@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'admin@gmail.com',
      password: 'pass1234',
    },
    {
      id: 2,
      email: 'user1@gmail.com',
      password: 'pass1234',
    },
  ];

  async findOne(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email);
  }
}
